// CE VOM INVATA:
// Tipuri de date: Primitive si obiecte
// Primite: Number, string (templare strings ES6), boolean, undefined, null, symbol

//  var a = 3;
// var b = 3.12;
// console.log(a, b);
const x = 1;
const y = 2;
console.log(x + y);

var c = "training" + 3; // folosind " putem concatena cu +
var d = "1" + 3; // string + int = string
var e = `misto ${2 + 3}`; // folosind ` putem concatena cu ${}

//mdn math
console.log(Math.floor(3.14));
console.log(Math.floor(Math.random) * 100);

var varsta = 22;
var anStudiu = 3;
var facultate = "CSIE";

console.log(facultate.length);

var adevarat = true;
var negativ = false;

var p; // undefined;
var l = null; 
var Tiberiu = "Tibi are " + varsta + " de ani" + " si e la facultatea " + facultate;
var TiberiuSimplu = `Tibi are ${varsta} de ani si e la facultatea ${facultate}`;

console.log("training", c);
console.log("numar ", d);
console.log("e: ", e);

// Variabile (var, let si const)

for(var i = 0; i < 10; i++) {
    console.log(i);
}

console.log("in afara scopului ", i);

console.log(a);
var a = 3;
var a = 10; //putem redefini var-ul
var a = "string";   
console.log(a);

//let, const

//const a = 3;
//a = 5; // nu este posibil

console.log(b); // nu putem folosi b-ul inainte de a-l defini
let b = a;
b = 7;
//let b = 10; // nu putem redefini let-ul

//printr-un bloc inteleg ce se afla in interiorul acoladelor (if, while, for, functii etc)
//putem inconura tot blocul js cu acolade pentru a nu ptea apela variabile din cod in consola din browser
const x = 20;
if(true) { // const sunt definite local in if, nu vor putea fi accesate inafara (local scope)
    const x = 10;
    const y = 20;
}

console.log(x, y); // vom avea valorile 10 pentru x si pentru y vom primi eroare

// Operatori de atribuire, de comparatie, logici, operator ternar)

let a = 10;
let b = a + 3;
let c = b++;
let d = a - 10;
let e = a--;
console.log(a, b, c, d, e);
console.log(3 < 5);
console.log(3 > 5); // >= <=
console.log("1" == 1); // rezultat true ca nu compara si tipul
console.log("1" === 1); // rezultat false pentru ca compara si tipul variabilelor
const z = 3 > 5; // false
console.log(!z); // va afisa true
console.log(3 < 5 && 2 > 5); // false
console.log(3 < 5 || 2 > 5); // true

// Structuri conditionate (if, else, switch)
const a = 10;
if(a > 5) {
    console.log("a e mai mare decat 5");
} else if(a > 3) {
    console.log("a e mai mare decat 3");
} else {
    console.log("a nu e mai mare ca 3");
}

let b = 0;
a > 5 ? b = 10 : b = -10;

console.log(b);

switch(a) {
    case 10:
        console.log("a este 10");
        break;
    case 8:
        console.log("a este 8");
        break;
    default:
        console.log("a este altceva");  

}
// Structuri repetitive (for, while)

for(let i = 0; i < 10; i++) { // let definest ca local scope
    console.log(i);
}
//console.log(i); // o sa dea eroare ca i nu e definit 

let a = 0;
while(a < 10) {
    console.log(a);
    a = a + 2;
}

// Functii (nomrlae si arrow functions)

function suma(a, b) {
    //console.log(a + b);
    return a + b;
}

const x = suma(3, 10);
suma(3, 20);

// const sumaDoi = (a, b) => { // arrow function sau lambda function
//     console.log(a + b);
// };
// SAU
const sumaDoi = (a, b) => a + b;

const y = sumaDoi(5, 10);

console.log(x, y);

sumaDoi(5, 10);
// Array-uri (metode pe array-uri -> pop, push, shift, unshift, forEach, map, filter
//     + spread operator

let a = [1, 4, 2, "string", 15, undefined, true];
console.log([a.length - 1]);

a.push("sfarsit"); // adaugam la sfarsitul vectorului
console.log(a);

a.pop(); // scoatem de la sfarsitul vectorului
console.log(a);

a.unshift(3, 10); // adauga la inceput, in exemplu nostru adauga 3 si 10
console.log(a);
a.shift(); // scoate primul element al vectorului

a.slice(2, 4); // luam elementele de pe pozitia 2 si 4 (exclusiv) (vom avea doar elementele de pe pozitia 2 si 3)

//mdn array -> pentru mai multe metode cu vectori

for(let i = 0; i < a.length; i++) {
    console.log(a[i]);
}

//SAU

a.forEach(el => {
    console.log(el);
});

//SAU

const afisareElement = el => {
    console.log(el);
};

a.forEach(afisareElement);

//SAU 

const afisareElement = (el, index) => {
    a[index] = 1;
}

a.forEach(afisareElement);

console.log(a); 

const b = [1, 2, 3, 4, 5];
const c = b.map((el, index) => (el + 3) * index);
console.log("b: ", b);
console.log("c: ", c);

const c = b.filter(el => el > 3);
console.log(b, c);

//spread operator, mai jos ii spunem obiectului b sa se extinda cu elementele lui c
const d = [...b, ...c];
console.log(d);

const e = [...b, c, 10, 2]; // aici, elementele lui c ar fi un vector in vectorul lui e (un vector stand a lone)
console.log(e[5][0]);

// Obiecte (spread operator)

let Tibi = {
    varsta: 22,
    anStudiu: 3,
    facultate: "CSIE",
    specializare: "IE",
    hobbyuri: ["programare", "sport"],
    trasaturiFizice: {
        culoareOchi: "caprui",
        par: "saten"
    },
    esteMajor: function() {
        return this.varsta > 18;
    }
};

console.log(Tibi.varsta);
console.log(Tibi["anStudiu"]);
console.log(Tibi.esteMajor);

Tibi.trasaturiFizice.inaltime = 175; // putem adauga propietati obiectului pe parcurs
Tibi.note = "7";
console.log(Tibi); 

const TibiDoi = {
    ...Tibi, // extindem obiectul Tibi
    note: "7",
    varsta: 25 // suprascriem varsta
}

Tibi.varsta = 28;

console.log(TibiDoi);

delete Tibi.hobbyuri; // stergem o propietate a obiectului

// console.log(window); // ne arata in consola cu ce putem lucra / modifica
// console.log(document);

