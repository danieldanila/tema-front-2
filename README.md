# Tema-Front-2

## Aspectul paginii / Apăsarea butonului „All”

![](readmeImages/all.png) 

## Apăsarea butonului „Lactate”

![](readmeImages/lactate.png) 

## Apăsarea butonului „Carne”

![](readmeImages/carne.png) 

## Apăsarea butonului „Sucuri”

![](readmeImages/sucuri.png) 

## Probleme
(Cu toate că are această problemă, funcționalitatea de filter functioneză cum trebuie) <br />
La apăsarea unui buton de filter, se ajunge într-un for care iterează mai mult decât trebuie astfel încât itemNamesList[j].style are valorea „undefined” <br />

Eroarea din consola: <br />
![](readmeImages/consola.png) 

Secvența de cod unde se produce eroarea: <br />
![](readmeImages/cod.png) 

