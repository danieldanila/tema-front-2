const button = document.getElementById("enter"); //folosin clasa buton ca sa selectez elemente din document (html)
const input = document.getElementById("userInput");
// const ul = document.querySelector("ul"); // aici ne va lua toate ul-urile din html, daca puneam #ul, ne lua id ul daca exista, si daca puneam .ul ne lua clasa ul, daca exista
const shoppingListUl = document.getElementById("shopping-list");

// const listItems = document.querySelector("li");
//console.log(button); // ne va afisa butonul, adica codul html al butonului <button id=....></button>

// Tema: selectati DROPDOWN-ul din html si butoanele de filtrare
const optionsSelect = document.getElementById("options");
const filterUl = document.getElementById("filter");

function inputLength() {
    return input.value.length;
}

function createListElement() {
    const li = document.createElement("li");
    const span = document.createElement("span");
    const icon = document.createElement("i");

    icon.classList.add("fas", "fa-trash");
    span.className = "shopping-item-name";
    span.textContent = input.value;

    li.appendChild(span); // adaugam in DOM ce am creat pana acum
    li.appendChild(icon);

    // Tema: adaugati noului li creat creat clasa corespunzatoare categoriei din care face parte
    li.classList.add(optionsSelect.options[optionsSelect.selectedIndex].text.toLowerCase());

    shoppingListUl.appendChild(li); // acum o sa apara si in document

    input.value = ""; // dupa ce dam enter vrem sa dispara ce am scris in casuta
    //daca vrem ca valorile adaugate aici sa ramana pe pagina dupa refresh, trebuie sa
    // ne folosim de local storage
}

function deleteListElement(target) {
    // target e un parametru si in cazul nostru va primi event.target
    // target.parentElement, ne va retruna clasa target-ului, in cazul nostru mereu va fi parintele clasei fa-trash, adica li
    shoppingListUl.removeChild(target.parentElement); // sterge copilul lui ul, adica li-ul
} 

function addListItemAfterClick() {
    if(inputLength() > 0) {
        createListElement();
        console.log("Hello");
    }
}

function addListItemAfterKeypress(event) { // event e o clasa din js
    console.log(event);
    if(inputLength() > 0 && event.key === "Enter") {
        createListElement();
    }
}

button.addEventListener("click", addListItemAfterClick);
input.addEventListener("keypress", addListItemAfterKeypress);
filterUl.addEventListener("click", filterItems);

document.addEventListener("click", event => {
    // event.target retine ce am apasat noi pe document 
    if(event.target.classList.contains("fa-trash")) {
        deleteListElement(event.target);
    } else if(event.target.classList.contains("shopping-item-name")) {
        event.target.classList.toggle("done");
    } else if(event.target.classList.contains("lactate") || event.target.classList.contains("carne") || event.target.classList.contains("sucuri")) {
        return event.target.id;
    } else {
        return;
    }
});

//Tema: functie numita filterItems in care sa filtrati elementele din pagina
function filterItems(event) {
    let itemsToBeDisplayed = [];
    let clickedItem = event.target;
    let itemNamesList = shoppingListUl.getElementsByTagName("li");
    
    for(let i in itemNamesList) {
        if(itemNamesList[i].className === clickedItem.className) {
            itemsToBeDisplayed.push(itemNamesList[i]);
        }        
    }       

    if(itemsToBeDisplayed.length > 0) {
        for(let i in itemsToBeDisplayed) {
            for(let j in itemNamesList) {
                if(itemsToBeDisplayed[i].className === itemNamesList[j].className) {
                    itemNamesList[j].style.display = "flex";
                } else if(clickedItem.className == "all") {
                    itemNamesList[j].style.display = "flex";
                } else {
                    itemNamesList[j].style.display = "none";
                }
            }          
        }
     } else {
        if(clickedItem.className == "all") {
            for(let j in itemNamesList) {
                itemNamesList[j].style.display = "flex";
            }
        } else {
            for(let j in itemNamesList) {
                itemNamesList[j].style.display = "none";
            }          
        }
     }
}
